package com.example.demo.domain;


import org.springframework.data.repository.CrudRepository;

public interface SaleRepository extends CrudRepository<Sale,Long>{

	 public default Iterable<Sale> getAll(){
		return this.findAll();
	 }
	 
	 public default void add(String shop,String item,Integer volume) {
		 Sale sale = new Sale();
		 sale.setShop(shop);
		 sale.setItem(item);
		 sale.setVolume(volume);
		 this.save(sale);
	 }
	 
}
