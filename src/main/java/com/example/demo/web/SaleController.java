package com.example.demo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Sale;
import com.example.demo.domain.SaleRepository;

@RestController
@RequestMapping("/")
public class SaleController {

	@Autowired
	private SaleRepository saleRepository;
	
	@RequestMapping("/index")
    public @ResponseBody Iterable<Sale> index() {
		return saleRepository.getAll();
    }
	
	@GetMapping(path="/add")
	public @ResponseBody String add(String shop,String item,Integer volume) {
		saleRepository.add(shop, item, volume);
		return "add successed";
	}
}
